# Argo 21 Numpad

![License CERN-OHL-S](https://img.shields.io/badge/LICENSE-CERN--OHL--S-blue?style=for-the-badge&logo=opensourceinitiative) ![Powered by QMK](https://img.shields.io/badge/Powered%20by-QMK-lightgrey?style=for-the-badge)

A custom mechanical numpad design with a rotary encoder, a slider, and an extra row of 4 keys. It also has per-key addressable RGB lighting. Any microcontroller board that is pin-compatible with the Arduino Pro Micro should *theoretically* work with it as long as QMK can be used.

**NOTE: This is a work in progress! Expect major changes.**

This is the current layout of the numpad, excluding the encoder and slider.
![Key Layout](layout.png)
